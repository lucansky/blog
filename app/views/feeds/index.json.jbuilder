json.array!(@feeds) do |feed|
  json.extract! feed, :id, :title, :body
  json.url feed_url(feed, format: :json)
end
